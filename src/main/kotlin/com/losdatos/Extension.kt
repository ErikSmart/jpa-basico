package com.losdatos

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.util.*

fun Any?.writeValueAsString(): String? {
    return if (this != null) {
        val mapper = jacksonObjectMapper()
        mapper.writeValueAsString(this)
    } else {
        null
    }
}

fun String?.toStringArray(): Collection<String>? {
    return if (this != null && this.isNotEmpty()) {
        val mapper = jacksonObjectMapper()
        return try {
            mapper.readValue(this)
        } catch (e: Exception) {
            null
        }
    } else {
        null
    }
}

fun String?.toMap(): Map<String, Any>? {
    return if (this != null && this.isNotEmpty()) {
        val mapper = jacksonObjectMapper()
        return try {
            mapper.readValue(this)
        } catch (e: Exception) {
            null
        }
    } else {
        null
    }
}

// kudos to https://gist.github.com/maiconhellmann/796debb4007139d50e39f139be08811c
fun Date.add(field: Int, amount: Int): Date {
    Calendar.getInstance().apply {
        time = this@add
        add(field, amount)
        return time
    }

    /*
    * TRUNCATE TABLE app_role;
INSERT INTO app_role (id, role_name, description)
VALUES (1, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');
INSERT INTO app_role (id, role_name, description)
VALUES (2, 'STANDARD_USER', 'Standard User - Has no admin rights');

TRUNCATE TABLE app_user;
-- password test1234
INSERT INTO app_user (id, first_name, last_name, password, username)
VALUES (1, 'Admin', 'Admin', '$2a$10$5AWyzymSnNypg9BkMOyKE.zA05GtRKHCoWimh.q2w.KAO5koBYPM6', 'admin.admin');
-- password test1234
INSERT INTO app_user (id, first_name, last_name, password, username)
VALUES (2, 'John', 'Doe', '$2a$10$5AWyzymSnNypg9BkMOyKE.zA05GtRKHCoWimh.q2w.KAO5koBYPM6', 'john.doe');

TRUNCATE TABLE user_role;
INSERT INTO user_role(user_id, role_id)
VALUES (1, 1);
INSERT INTO user_role(user_id, role_id)
VALUES (1, 2);
INSERT INTO user_role(user_id, role_id)
VALUES (2, 2);
    * */
}
