package com.losdatos.securingweb

class Constants() {


    // Spring Security

    // Spring Security
    var LOGIN_URL = "/login"
    var HEADER_AUTHORIZACION_KEY = "Authorization"
    var TOKEN_BEARER_PREFIX = "Bearer "

    // JWT

    // JWT
    var ISSUER_INFO = "https://www.autentia.com/"
    var SUPER_SECRET_KEY = "1234"
    var TOKEN_EXPIRATION_TIME: Long = 864000000 // 10 day

    constructor(LOGIN_URL: String, HEADER_AUTHORIZACION_KEY: String, TOKEN_BEARER_PREFIX: String, ISSUER_INFO: String, SUPER_SECRET_KEY: String, TOKEN_EXPIRATION_TIME: Long):this() {
        this.LOGIN_URL = LOGIN_URL
        this.HEADER_AUTHORIZACION_KEY = HEADER_AUTHORIZACION_KEY
        this.TOKEN_BEARER_PREFIX = TOKEN_BEARER_PREFIX
        this.ISSUER_INFO = ISSUER_INFO
        this.SUPER_SECRET_KEY = SUPER_SECRET_KEY
        this.TOKEN_EXPIRATION_TIME = TOKEN_EXPIRATION_TIME
    }
}