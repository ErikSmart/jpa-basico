package com.losdatos.misdatos

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@SpringBootApplication(exclude = [(RepositoryRestMvcAutoConfiguration::class)])
@ComponentScan(basePackages=arrayOf("com.losdatos"))
@EntityScan("com.losdatos.modelo")
@EnableJpaRepositories("com.losdatos.repository")

class MisdatosApplication

fun main(args: Array<String>) {
	runApplication<MisdatosApplication>(*args)
}
