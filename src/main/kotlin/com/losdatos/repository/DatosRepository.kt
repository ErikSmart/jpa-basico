package com.losdatos.repository

import com.losdatos.modelo.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface DatosRepository: JpaRepository<Modelo,String> {

}


@Repository
interface EmpleadoRepository: JpaRepository<Empleado,String> {
    @Query(
            value = "SELECT * FROM Empleado ORDER BY id",
            countQuery = "SELECT count(*) FROM Empleado",
            nativeQuery = true)
     fun findAllEmpleadoWithPagination(pageable: Pageable): List<Empleado>
}

@Repository
interface paginacionRepository : PagingAndSortingRepository<Empleado, String> {
override fun findAll(pageable: Pageable): Page<Empleado>
}

@Repository
interface ActoresRepository: JpaRepository<Actores,String> {
}

@Repository
interface PeliculaRepository: JpaRepository<Peliculas,String> {
}

@Repository
interface UsuarioRepository: JpaRepository<Usuario,Long>{
    fun findByUsername(username: String): Optional<Usuario>
}