//Este controller actua mas como un enrutulador y el servicio es mi logica de negocio
package com.losdatos.controller

import com.losdatos.modelo.Empleado
import com.losdatos.modelo.Modelo
import com.losdatos.repository.DatosRepository
import com.losdatos.repository.EmpleadoRepository
import com.losdatos.repository.paginacionRepository
import com.losdatos.servicio.ContactoServicio
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.Query
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import javax.persistence.EntityManagerFactory
import javax.print.attribute.standard.PageRanges

@PreAuthorize("authenticated")
@RestController
@RequestMapping("/api/v1")
class DatosController {
    @Autowired
private lateinit var  manager : EntityManagerFactory;
@Autowired
private lateinit  var datosrepository : DatosRepository
    @Autowired
    private  lateinit var contactoServicio: ContactoServicio
    @Autowired
    private  lateinit var empleadoRepository: EmpleadoRepository
    @Autowired
    private  lateinit var paginacionRepository: paginacionRepository

    @PreAuthorize("hasRole('ADMIN_USER')")
    @GetMapping("/")

    fun saludo(): String  { return "Esta funcionado Kotlin" }

    @GetMapping("/restricted")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    fun probando () :String { return contactoServicio.saludo() }

    @GetMapping("/contacto")
    fun inicio() = contactoServicio.inicio()
    //@PostMapping("/contacto")
   // fun crear(@RequestBody modelo: Modelo) = datosrepository.save(modelo)
    //Crear usuario manualmente
    @GetMapping("/crearmanual")
    fun manual(){
        var entity = manager.createEntityManager()
        var insertar : Modelo = Modelo("Juan","juan@febles.com")
        entity.transaction.begin()
        var cambiar = entity.find(Modelo::class.java,6)
        cambiar.nombre ="Julia"
        cambiar.correo ="julia@msn.com"
        //Eliminar
        //entity.remove(cambiar)
        entity.transaction.commit()
    }
    @GetMapping("/buscar")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    fun buscar()= contactoServicio.buscar()


    @GetMapping("/borrar")
    fun borrar() {
        return contactoServicio.borrar()
    }

    @PostMapping("/contacto")
    fun crear(@RequestBody modelo: Modelo) = datosrepository.save(modelo)

    @GetMapping("/contacto/{id}")
        fun individual (@PathVariable id: String): Modelo? {
        return contactoServicio.findByIdd(id)
    }

    @GetMapping("/mapa")
    fun insertar() = contactoServicio.crearPeliculaActor()

    @GetMapping("/mostrar")
    fun m()= contactoServicio.listarActores()

    @GetMapping("/listar")
    fun l()= contactoServicio.mostrarActores()

    @RequestMapping("/hola" )
    fun pagina(modelAndView: ModelAndView ): ModelAndView {
        modelAndView.viewName = "hola"
        return modelAndView
    }

    @GetMapping("/inter")

    fun inter(page: Pageable) =  paginacionRepository.findAll(PageRequest.of(1,2)).content.sortedByDescending { x-> x.id }            //paginacionRepository.findAll(PageRequest.of(1,2))
            //empleadoRepository.findAllEmpleadoWithPagination(PageRequest.of(1,2))
}