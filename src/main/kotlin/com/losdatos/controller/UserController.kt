/*
package com.losdatos.controller

import com.losdatos.dto.User
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
class UserController {
    @PostMapping("/user")
    fun login(@RequestParam("user") username: String, @RequestParam("password") pwd: String): User {
        var token: String = getJWTToken(username);
        var user: User = User()
        user.user=username;
        user.pwd=pwd
        user.token =token;
        return user
    }

    fun getJWTToken(username: String): String {
        var secretKey: String = "mySecretKey"
        var grantedAuthorities: MutableList<GrantedAuthority>? = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER")
        var token: String = Jwts.builder()
                .setId("softtekJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities?.stream()
                                ?.map(GrantedAuthority::getAuthority)
                                ?.collect(Collectors.toList()))
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.toByteArray()).compact();

        return "Bearer " + token;
    }

}*/
