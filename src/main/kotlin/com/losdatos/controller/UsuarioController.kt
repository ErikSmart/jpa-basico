package com.losdatos.controller

import com.losdatos.modelo.Usuario
import com.losdatos.repository.UsuarioRepository
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import java.util.*


class UsuarioController {
    private var usuarioRepository: UsuarioRepository? = null

    private var bCryptPasswordEncoder: BCryptPasswordEncoder? = null

    fun UsuarioController(usuarioRepository: UsuarioRepository?, bCryptPasswordEncoder: BCryptPasswordEncoder?) {
        this.usuarioRepository = usuarioRepository
        this.bCryptPasswordEncoder = bCryptPasswordEncoder
    }

    @PostMapping("/users/")
    fun saveUsuario(@RequestBody user: Usuario) {
        user.password = bCryptPasswordEncoder!!.encode(user.password)
        usuarioRepository!!.save(user)
    }

    @GetMapping("/users/")
    fun getAllUsuarios(): List<Usuario?>? {
        return usuarioRepository!!.findAll()
    }

    @GetMapping("/users/{username}")
    fun getUsuario(@PathVariable username: String?): Optional<Usuario>? {
        return username?.let { usuarioRepository!!.findByUsername(it) }
    }
}