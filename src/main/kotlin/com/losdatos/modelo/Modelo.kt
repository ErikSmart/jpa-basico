package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*


@Entity
@Table(name = "modelo")
class Modelo() {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: String = "";
    var nombre: String = ""
    var correo: String = ""

    @OneToOne(cascade = arrayOf(CascadeType.ALL))
    @JoinColumn(name = "id_d")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var direccion: Direccion ?= null

    constructor(nombre: String, correo: String):this() {
        this.nombre=nombre
        this.correo=correo
    }
}