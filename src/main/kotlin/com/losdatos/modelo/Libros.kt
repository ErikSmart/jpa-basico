package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
@Table(name = "libros")
class Libros {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0;
    var titulo: String = ""
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "autores_id", nullable = false)
    val autores: Autores ?= null

}