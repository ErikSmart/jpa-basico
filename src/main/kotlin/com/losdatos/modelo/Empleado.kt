package com.losdatos.modelo

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@Table(name = "empleado")
class Empleado() {
    @Id
    var id = 0
    @Column(columnDefinition = "varchar(30)")
    var nombre : String = ""
    @Column(columnDefinition = "varchar(40)")
    var paterno: String = ""
    @Column(columnDefinition = "varchar(30)")
    var materno: String = ""

    @OneToOne(mappedBy = "empleado")
    //@JsonManagedReference
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var domicilio: Domicilio?= null

    constructor(id: Int,nombre: String, paterno: String, materno:String):this(){
        this.id=id
        this.nombre=nombre
        this.paterno=paterno
        this.materno=materno
    }

    override fun toString(): String {
        return "Empleado(id=$id, nombre='$nombre', paterno='$paterno', materno='$materno', domicilio=$domicilio)"
    }


}