package com.losdatos.modelo

import javax.persistence.*

@Entity
@Table(name = "app_role")
class Role() {
        @Id
        @GeneratedValue()
        var id: Int = 0

        @Column(name = "role_name")
        var roleName: String? = null

        @Column(name = "description")
        var description: String? = null

        constructor(id: Int, roleName: String?, description: String?):this() {
                this.id = id
                this.roleName = roleName
                this.description = description
        }
}