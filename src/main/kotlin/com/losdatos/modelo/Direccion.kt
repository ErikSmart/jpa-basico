package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*


@Entity
@Table(name = "direccion")
class Direccion {
    @Id
    var id: Int = 0
    var calle: String = ""
    var numero: Int = 0
    @OneToOne(mappedBy = "direccion")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY) es para evitar desvordamiento infinito
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var modelo: Modelo ?= null

}