package com.losdatos.modelo

import javax.persistence.*

@Entity
@Table(name = "autores")
class Autores() {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0
    var nombre: String = ""
    var nacionalidad: String = ""
    @OneToMany(mappedBy = "autores",cascade = [CascadeType.ALL])
    val libros : List<Libros> = emptyList()

    constructor(id: Int, nombre: String, nacionalidad: String):this() {
        this.id = id
        this.nombre = nombre
        this.nacionalidad = nacionalidad
    }

}