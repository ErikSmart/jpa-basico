package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
@Table(name = "actores")
//@SecondaryTable(name = "domicilio",pkJoinColumns = arrayOf(PrimaryKeyJoinColumn(name = "empleado", referencedColumnName = "id")))
class Actores() {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0
    var nombre: String = ""
    @OneToOne
    @Embedded
    var domicilio: Domicilio = Domicilio()
//Fetch lazy es la carga bajo demanda
    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST])
    // JoinTable es para crear hacer la union de las tablas joinColumns e inverseJoinColumn name es la tabla intermedia de ManytoMany
            @JoinTable(
            name = "Actores_Peliculas",
            joinColumns = arrayOf(JoinColumn(name = "actor_id")),
            inverseJoinColumns = arrayOf(JoinColumn(name = "pelicula_id")))
    var peliculas: List<Peliculas> = mutableListOf<Peliculas>()



    constructor(nombre: String, peliculas: List<Peliculas>):this() {
        this.nombre = nombre
        this.peliculas = peliculas
    }

    override fun toString(): String {
        return "Actores(id=$id, nombre='$nombre', peliculas=$peliculas)"
    }
}