package com.losdatos.modelo


import javax.persistence.*

@Entity
@Table(name = "domicilio")
@Embeddable
class Domicilio() {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0
    var pais: String = ""
    @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "empleado")
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var empleado: Empleado ?= null


    constructor(id: Int, pais: String, empleado: Empleado?):this() {
        this.id = id
        this.pais = pais
        this.empleado = empleado

    }

    override fun toString(): String {
        return "Domicilio(id=$id, pais='$pais', empleado=$empleado)"
    }

}





