package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity
import javax.persistence.Id


@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
class CreadoConVistaSQL()  {
    @Id
    @JsonIgnore
    var id: Long = 0;
    var nombre: String =""
    var titulo: String = ""
    var pais: String = ""

    constructor(nombre: String, titulo: String, pais: String):this() {
        this.nombre = nombre
        this.titulo = titulo
        this.pais = pais
    }

    override fun toString(): String {
        return "Prueba(nombres='$nombre', titulo='$titulo', pais='$pais')"
    }


}