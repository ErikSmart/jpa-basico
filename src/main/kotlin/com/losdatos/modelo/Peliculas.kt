package com.losdatos.modelo

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
class Peliculas() {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    var id: Int = 0
    var titulo: String = ""
    @ManyToMany(mappedBy = "peliculas", cascade = [CascadeType.PERSIST], fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var actores: List<Actores> = mutableListOf<Actores>()

    constructor(titulo: String):this() {
        this.titulo = titulo
    }
}