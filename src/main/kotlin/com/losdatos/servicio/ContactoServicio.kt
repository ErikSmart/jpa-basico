package com.losdatos.servicio

import com.fasterxml.jackson.databind.ObjectMapper
import com.losdatos.modelo.*

import com.losdatos.repository.ActoresRepository
import com.losdatos.repository.DatosRepository
import com.losdatos.repository.PeliculaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

import javax.persistence.EntityManagerFactory


@Service
class ContactoServicio(private var datosrepository: DatosRepository, private var actoresRepository: ActoresRepository, private var peliculaRepository: PeliculaRepository):IAcciones<Modelo,String> {
    @Autowired
    private lateinit var  manager : EntityManagerFactory;
    fun saludo(): String {
        return "hola soy un servicio"
    }

    fun inicio() = datosrepository.findAll()

    //Lista a todos los autores
    fun buscar(): MutableList<Any?>? {
        var  mapper = ObjectMapper()

        var entity = manager.createEntityManager()
        //val query = entity.criteriaBuilder.createQuery(Modelo::class.java)
        //var otro = query.select(query.from(Modelo::class.java))
         var buscar = entity.createQuery("from Autores").resultList

        /*var encontrar = entity.createNativeQuery("select cast(empleado.id as varchar),nombre,pais from empleado join domicilio on empleado.id=domicilio.empleado").resultList

          val resultado = encontrar.map {
              it ->
              val lst = it as Array<out Any>
              Modelo(
                      lst[0] as String,
                      lst[1] as String

              )
          }
        */
        return buscar
        entity.close()
    }
    //Borra a autores
    fun borrar(): Unit{
        var  mapper = ObjectMapper()

        var entity = manager.createEntityManager()
        //val query = entity.criteriaBuilder.createQuery(Modelo::class.java)
        //var otro = query.select(query.from(Modelo::class.java))
        entity.transaction.begin()
        var eli = entity.find(Autores::class.java,9);
        eli.id=9
        var borrar = entity.remove(eli)
        entity.transaction.commit()
        return  borrar
        entity.close()
    }
    //Crea manualmente a los autores y peliculas y se registran tabmbien en la tabla intermedia
    fun crearPeliculaActor(){
        var entity = manager.createEntityManager()
        var anillos: Peliculas = Peliculas("El señor de los anillos");
        var mafia : Peliculas = Peliculas("El padrino")
        var alfred: Actores = Actores("Alfred Loco", listOf(mafia))
        var juan: Actores = Actores("Juan Alcalaz", listOf(anillos))

        entity.transaction.begin()
        entity.persist(alfred)
        entity.transaction.commit()
    }

    fun mostrarActores(): CreadoConVistaSQL? {
        var entity = manager.createEntityManager()
        //var res = entity.createQuery("select a from Actores a JOIN a.peliculas",Actores::class.java).resultList
        var res = entity.createQuery("select a FROM CreadoConVistaSQL a",CreadoConVistaSQL::class.java).singleResult
        /* var res = entity.createNativeQuery("Select * From Prueba").resultList

        val resultado = res.map {
            it ->
            val lst = it as Array<out Any>
            Prueba(
                    lst[0] as String,
                    lst[1] as String,
                    lst[2] as String

            )
        }

         */
        return res
    }

    fun listarActores(): MutableList<Actores>? {
        var entity = manager.createEntityManager()
        var res = actoresRepository.findAll()
        return res
    }

        override fun save(t: Modelo) {
            datosrepository.save(t)
        }


    override fun findByIdd(id: String): Modelo? {
      return  this.datosrepository.findByIdOrNull(id)
    }

}