package com.losdatos.servicio

interface IAcciones<T,ID> {
    fun save(t:T)
    fun findByIdd(id: ID): T?
}