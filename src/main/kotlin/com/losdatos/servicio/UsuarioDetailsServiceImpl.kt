package com.losdatos.servicio

import com.losdatos.repository.UsuarioRepository
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.util.ArrayList


@Component
class UsuarioDetailsServiceImpl(private val usuarioRepository: UsuarioRepository) : UserDetailsService {
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(s: String): UserDetails {
        val user = usuarioRepository.findByUsername(s)
                .orElseThrow { UsernameNotFoundException("The username $s doesn't exist") }

        val authorities = ArrayList<GrantedAuthority>()
        user.roles!!.forEach { authorities.add(SimpleGrantedAuthority(it.roleName)) }

        return User(
                user.username,
                user.password,
                authorities
        )
    }

}